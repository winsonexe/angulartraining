import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-country-info',
  templateUrl: './country-info.component.html',
  styleUrls: ['./country-info.component.css']
})
export class CountryInfoComponent implements OnInit {
  countrysData: any[] = [];
  @Input() filterData: string='';
  sortType: string='asc';
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getCountrysData();
  }

  getCountrysData() {
    this.http.get<any>("https://restcountries.eu/rest/v2/all")
      .subscribe(response => {
        this.countrysData = response;
      });
  }

  filterList(){
    return this.countrysData
    .filter(countryInfo =>
      {
        return countryInfo.name.toLowerCase().includes(this.filterData.toLowerCase())
      })
  }
}
