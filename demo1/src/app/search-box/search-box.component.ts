import { Component, OnInit, Output, EventEmitter,Input} from '@angular/core';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent implements OnInit {
  keyword!: string ;
  @Output() keywordChange=new EventEmitter<string>();
  sortType: string='asc';

  constructor() { }

  ngOnInit(): void {
  }

  search(){
    this.keywordChange.emit(this.keyword);
  }

  sortCountry() {
    if (this.sortType === 'asc') {
      this.sortType = 'desc';
    }
    else if (this.sortType === 'desc') {
      this.sortType = 'asc';
    }
  }

}
